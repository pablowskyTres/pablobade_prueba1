package com.test.pablobade.pablobade_prueba1;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class LoginPacienteActivity extends AppCompatActivity {

    private EditText etUser, etPass;
    private Button btnLogin, btnCallCenter;
    private ArrayList<Usuario> listUsuarios;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
    }

    private void initView(){
        listUsuarios = new ArrayList<Usuario>();
        listUsuarios.add(new Usuario("admin", "admin"));
        listUsuarios.add(new Usuario("root", "root"));
        listUsuarios.add(new Usuario("morty", "morty"));

        etUser = (EditText) findViewById(R.id.etUser);
        etPass = (EditText) findViewById(R.id.etPass);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnCallCenter = (Button) findViewById(R.id.btnCallCenter);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginControl();
            }
        });

        btnCallCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llamarCallCenter();
            }
        });
    }

    private void llamarCallCenter() {
        Intent i = new Intent(android.content.Intent.ACTION_DIAL,
        Uri.parse("tel:+569 555-55-55"));
        startActivity(i);
    }

    private void loginControl(){
        if (loginPaciente()){
            Intent i = new Intent(this, TomarHoraMedicaActivity.class);
            startActivity(i);
            finish();
        }else{
            Toast toast = Toast.makeText(this, "Usuario o Clave incorrectos", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private boolean loginPaciente() {
        String user = etUser.getText().toString();
        String pass = etPass.getText().toString();

        for (int i = 0; i < listUsuarios.size(); i++){
            if (listUsuarios.get(i).getUser().equals(user) && listUsuarios.get(i).getPass().equals(pass)){
                return true;
            }
        }
        return false;
    }
}
