package com.test.pablobade.pablobade_prueba1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.Calendar;
import java.util.Date;

public class TomarHoraMedicaActivity extends AppCompatActivity {

    private EditText etNombre, etFecha, etHora, etTelefono;
    private Button btnGuardar, btnLimpiar, btnListado, btnCerrarSesion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tomar_hora_medica);
        initView();
    }

    private void initView() {
        etNombre    = (EditText) findViewById(R.id.etNombre);
        etFecha     = (EditText) findViewById(R.id.etFecha);
        etHora      = (EditText) findViewById(R.id.etHora);
        etTelefono  = (EditText) findViewById(R.id.etTelefono);

        btnGuardar      = (Button) findViewById(R.id.btnGuardar);
        btnLimpiar      = (Button) findViewById(R.id.btnLimpiar);
        btnListado      = (Button) findViewById(R.id.btnListado);
        btnCerrarSesion = (Button) findViewById(R.id.btnCerrarSesion);

        btnCerrarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cerrarSesion();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tomarHora();
            }
        });

        btnListado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listado();
            }
        });
    }

    private void listado() {
        Intent i = new Intent(this, ListadoDeHorasTomadasActivity.class);
        startActivity(i);
        finish();
    }

    private void tomarHora() {
        String nombre = etNombre.getText().toString();
        String fecha = etFecha.getText().toString();
        String hora = etHora.getText().toString();
        String tel = etTelefono.getText().toString();

        String mensaje;

        try {
            if (nombre.equals("")){
                mensaje = "Debe ingresar un nombre";
            }else{
                if (fecha.equals("")){
                    mensaje = "Debe ingresar una fecha";
                }else{
                    if (hora.equals("")){
                        mensaje = "Debe ingresar una hora";
                    }else{
                        if (tel.equals("")){
                            mensaje = "Debe ingresar un telefono";
                        }else{
                            HoraMedica hm = new HoraMedica(nombre, fecha, hora, tel);
                            if (AdministrarHorasMedicas.getInstance().guardarHoraMedica(hm)){
                                mensaje = "Hora Tomada: " + nombre + " " + fecha + " " + hora + " " + tel;
                            }else{
                                mensaje = "Hora médica duplicada";
                            }
                        }
                    }
                }
            }

        }catch (Exception ex){
            mensaje = ex.getMessage().toString();
        }

        Toast toast = Toast.makeText(this, mensaje, Toast.LENGTH_SHORT);
        toast.show();
    }

    private void limpiar() {
        etNombre.setText("");
        etFecha.setText("");
        etHora.setText("");
        etHora.getText().clear();
        etTelefono.setText("");
    }

    private void cerrarSesion() {
        Intent i = new Intent(this, LoginPacienteActivity.class);
        startActivity(i);
        finish();
    }
}
