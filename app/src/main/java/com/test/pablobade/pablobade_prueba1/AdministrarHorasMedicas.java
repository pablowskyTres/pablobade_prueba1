package com.test.pablobade.pablobade_prueba1;

import java.util.ArrayList;

/**
 * Created by Pablo on 23-09-2017.
 */

public class AdministrarHorasMedicas {
    private static AdministrarHorasMedicas instance;

    private ArrayList<HoraMedica> values;

    protected AdministrarHorasMedicas() {
        values = new ArrayList<>();
    }

    public static AdministrarHorasMedicas getInstance() {
        if (instance == null) {
            instance = new AdministrarHorasMedicas();
        }
        return instance;
    }

    public boolean guardarHoraMedica(HoraMedica horaMedica) {
        if(!buscarHoraMedica(horaMedica)) {
            values.add(horaMedica);
            return true;
        }else {
            return false;
        }
    }

    public boolean buscarHoraMedica(HoraMedica horaMedica) {
        for (HoraMedica aux : values) {
            if (horaMedica.getNombrePaciente().equals(aux.getNombrePaciente()) && horaMedica.getFecha().equals(aux.getFecha()) && horaMedica.getHora().equals(aux.getHora())) {
                return true;
            }
        }
        return false;
    }

    public ArrayList<HoraMedica> listarHorasMedicas(){
        return values;
    }
}
