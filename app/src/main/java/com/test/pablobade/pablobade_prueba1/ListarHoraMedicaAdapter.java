package com.test.pablobade.pablobade_prueba1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Pablo on 23-09-2017.
 */

public class ListarHoraMedicaAdapter extends BaseAdapter {
    private ArrayList<HoraMedica> values;
    private Context context;

    public ListarHoraMedicaAdapter(ArrayList<HoraMedica> values, Context context) {
        this.values = values;
        this.context = context;
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Object getItem(int position) {
        return values.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


            convertView = inflater.inflate(R.layout.item_hora_medica, parent, false);

            holder = new ViewHolder();
            holder.tvNombre = (TextView) convertView.findViewById(R.id.tvNombre);
            holder.tvFecha = (TextView) convertView.findViewById(R.id.tvFecha);
            holder.tvHora = (TextView) convertView.findViewById(R.id.tvHora);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        HoraMedica aux = values.get(position);
        holder.tvNombre.setText(aux.getNombrePaciente());
        holder.tvFecha.setText(aux.getFecha());
        holder.tvHora.setText(aux.getHora());

        return convertView;
    }

    private static class ViewHolder {
        TextView tvNombre;
        TextView tvFecha;
        TextView tvHora;
    }
}
