package com.test.pablobade.pablobade_prueba1;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Pablo on 23-09-2017.
 */

public class HoraMedica {
    private String nombrePaciente;
    private String fecha;
    private String hora;
    private String telefono;

    public HoraMedica(String nombrePaciente, String fecha, String hora, String telefono) {
        this.nombrePaciente = nombrePaciente;
        this.fecha = fecha;
        this.hora = hora;
        this.telefono = telefono;
    }

    public String getNombrePaciente() {
        return nombrePaciente;
    }

    public void setNombrePaciente(String nombrePaciente) {
        this.nombrePaciente = nombrePaciente;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
}
