package com.test.pablobade.pablobade_prueba1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

public class ListadoDeHorasTomadasActivity extends AppCompatActivity {

    private ListView lvHorasMedicas;
    private ListarHoraMedicaAdapter listarHoraMedicaAdapter;
    private Button btnVolver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_de_horas_tomadas);
        lvHorasMedicas = (ListView)findViewById(R.id.lvHorasMedicas);
        listarHoraMedicaAdapter = new ListarHoraMedicaAdapter(
                AdministrarHorasMedicas.getInstance().listarHorasMedicas(),
                this
        );
        lvHorasMedicas.setAdapter(listarHoraMedicaAdapter);

        btnVolver = (Button) findViewById(R.id.btnVolver);

        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                volver();
            }
        });
    }

    private void volver() {
        Intent i = new Intent(this, TomarHoraMedicaActivity.class);
        startActivity(i);
        finish();
    }
}
